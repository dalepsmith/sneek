; seen: Note and provide information on a users last message

(define-module (bot seen)
  #:use-module (srfi srfi-11)
  #:use-module (bot misc)
  #:use-module (bot sql)
  #:use-module (bot metaphone)
  #:use-module (bobotpp bot)
  #:use-module (ice-9 format)
  #:export (seen
            seenbot))

(define (date-components total-seconds)
  (let* ((s1 (inexact->exact (round total-seconds)))
         (years (quotient s1 31104000))
         (s2 (- s1 (* years 31104000)))
         (months (quotient s2 2592000))
         (s3 (- s2 (* months 2592000)))
         (days (quotient s3 86400))
         (s4 (- s3 (* days 86400)))
         (hours (quotient s4 3600))
         (s5 (- s4 (* hours 3600)))
         (minutes (quotient s5 60))
         (seconds (- s5 (* minutes 60))))
    (values years months days hours minutes seconds)))

(define (format-relatve total-seconds)
  (let-values (((years months days hours minutes seconds)
                (date-components total-seconds)))
    (cond
     ;; years
     ((> years 1)
      (format #f "~d years" years))
     ((= years 1)
      (if (= months 0)
          "one year"
          (format #f "one year and ~d month~:p" months)))

     ;; months
     ((> months 1)
      (format #f "~d months" months))
     ((= months 1)
      (if (= days 0)
          "one month"
          (format #f "one month and ~d day~:p" days)))

     ;; days
     ((> days 1)
      (format #f "~d days" days))
     ((= days 1)
      (if (= hours 0)
          "one day"
          (format #f "one day and ~d hour~:p" hours)))

     ;; hours
     ((>= hours 6)
      (format #f "~d hours" hours))
     ((>= hours 1)
      (if (= minutes 0)
	  (format #f "~d hour~:p" hours)
          (format #f "~d hour~:p and ~d minute~:p" hours minutes)))

     ;; minutes and seconds
     ((= minutes 1)
      (if (> seconds 0)
          (format #f "one minute and ~d second~:p" seconds)
          "one minute"))

     ((= minutes 0)
      (format #f "~d second~:p" seconds))

     ((= seconds 0)
      (format #f "~d minute~:p" minutes))

     (else
      (format #f "~d minute~:p and ~d second~:p" minutes seconds)))))


;; "Apr 15 at 06:33 pm" -> seconds (since jan 1 1970)
(define (old-date->seconds old-date)
  (define (convert year)
    (car (mktime (car (strptime "%Y %b %d at %I:%M %p"
                                (format #f "~a ~a" year old-date)))
                 "UTC")))
  (let* ((year (+ 1900 (tm:year (gmtime (current-time)))))
         (try (convert year)))
    (if (<= try (current-time))
        try
        (convert (- year 1)))))

(define (format-time seen-at)
  (format-relatve (- (current-time)
		     (or (string->number seen-at)
			 (old-date->seconds seen-at)))))
 
(define haventseen-responses
  '("Sorry, haven't seem 'em."
    "Nope."
    "Sorry, no."
    "Not as far as I can remember."))

(define seen-phrases
  ;; nick
  '("I last saw ~a"
    "I think I remember ~a"
    "~a?, pretty sure was seen"
    "~a was last seen"
    "~a was"))

(define (seen channel to message ignore term)
  (if term (set! term (string-trim-right term #\?)))
  (if (equal? ignore "")
      (let-values (((seen message person chan)
                    (lookup-seen (metaphone term))))
        ;; (format (bot:logport) "~s|~s|~s|~s~%" person chan seen message)
        ;; (bot:flushport)
        (if seen
	    (format #f "~@? in ~a ~a ago, saying: ~a."
                    (random-elem seen-phrases) person chan seen message)
            (random-elem haventseen-responses)))
      'continue))

(define (seenbot nick channel message)
  (store-seen (metaphone nick) nick channel message)
  #t)


;;CREATE TABLE seen (
;;    nick    NOT NULL,
;;    seenon  NOT NULL,
;;    message NOT NULL,
;;    id      NOT NULL,
;;    channel not NULL
;;);

(define (lookup-seen person)
  (let ((results (sql-query "SELECT seenon, message, nick, channel FROM seen WHERE id='~a'"
                            (fix person))))
    (if results
        (values (format-time (vector-ref (car results) 0)) ; seenon
                (vector-ref (car results) 1)               ; message
                (vector-ref (car results) 2)               ; nick
                (vector-ref (car results) 3))              ; channel
        (values #f #f #f #f))))


;; {NICK} was in {CHANNEL} {sometime} ago, saying: {MESSAGE}
(define (store-seen id nick channel message)
  (let-values (((a b c d) (lookup-seen id)))
    (let ((cmd (format #f
                       (if a
                           "UPDATE seen SET seenon='~a',message='~a',nick='~a',channel='~a' WHERE id='~a';"
                           "INSERT INTO seen(seenon,message,nick,channel,id) VALUES('~a','~a','~a','~a','~a');")
                       (current-time) (fix message) (fix nick) (fix channel) id)))
      ;; (display cmd (bot:logport))
      ;; (newline (bot:logport))
      ;; (bot:flushport)
      (sql-command cmd))))
