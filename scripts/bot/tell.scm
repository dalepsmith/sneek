; message delivery plugin

(define-module (bot tell)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-2)
  #:use-module (bot sql)
  #:use-module (bot metaphone)
  #:use-module (bobotpp bot)
  #:use-module (bot misc)
  #:use-module (ice-9 format)
  #:export (tell
            unawaybot))

(define (tell type)
  (lambda (from channel message ignore term)
    (store-tell from channel message term (eq? type 'later))))


(define (real-name nick) #f)

(define (store-tell from channel message term later)
  (and (not (equal? term ""))
       (and-let* ((spidx (string-index term #\space))
                  (recipient (substring term 0 spidx)))
                 (let ((todeliver (substring term (+ spidx 1)
                                             (string-length term))))
                   (if (not (equal? (string-ref todeliver 0) #\"))
                       (set! todeliver
                             (translate-for-third-party todeliver 'male)))
                   (cond ((equal? (metaphone recipient) (metaphone (bot:getnickname)))
                          (random-elem '("You just did."
                                         "Umm, I'm right here."
                                         "Weirdo.")))
                         ((and (not later)
                               ;;(not (null? (channels-user-occupies (metaphone recipient))))
                               )
                          (do-tell recipient
                                   channel ;; (car (channels-user-occupies (message-metaphone-nick message)))
                                   from ;; (message-nick message)
                                   todeliver)
                          #f)
                         (else
                          (store-message from ;; (message-nick message)
                                         (metaphone recipient)
                                         (string-downcase recipient)
                                         todeliver)
                          (random-elem tell-responses)))))))

(define (do-tell recipient channel-name sender message)
  (bot:say channel-name
           (format #f "~a, ~a says: ~a"
                   (or (real-name (metaphone recipient))
                       recipient)
                   sender message)))


(define (translate-for-third-party sentence sex) sentence)

;; This is the bad boy that keeps track of who needs mesages
(define (unawaybot channel to)
  (deliver-messages channel to)
  #t)


(define deliver-preludes
  '("~a, you have ~a message~:p!"
    "Welcome back ~a, you have ~a message~:p!"))

(define (deliver-messages channel sender) 
  (let ((messages (fetch-messages! (metaphone sender))))
    (if messages
        (begin
          (bot:say channel
                   (format #f
                           (random-elem deliver-preludes)
                           sender
                           (length messages)))
	  (for-each
	   (lambda (m)
	     (do-tell sender channel
                      (vector-ref m 0)
                      (vector-ref m 1)))
	   messages)))))


(define (store-message sender id recipient message)
  (sql-command "INSERT INTO tell(recipient,sender,message,id) VALUES('~a','~a','~a','~a');"
               recipient sender (fix message) id))


(define (fetch-messages! recipient)
  (let ((results (sql-query "SELECT sender, message FROM tell WHERE id='~a';"
                            recipient)))
    (and results
         (begin
           (sql-command "DELETE FROM tell WHERE id='~a';" recipient)
           (reverse results)))))


(define tell-responses
  '("Will do."
    "Got it."
    "Okay."))
