(define-module (bot infobot)
  #:use-module (bot info)
  #:use-module (bot misc)
  #:use-module (srfi srfi-11)
  #:use-module (srfi srfi-1)
  #:use-module (bot records)
  #:use-module (bot parsing)
  #:use-module (bot seen)
  #:use-module (bot tell)
  #:use-module (bobotpp bot)
  #:use-module (ice-9 session)
  #:use-module (ice-9 regex)
  #:export (make-standard-infobot))

(define max-help-lines 8)

(define-syntax make-infobot
  (syntax-rules ()
    ((_ (crib help handler) ...)
     (infobot (make-plugin crib help
                           (lambda args (apply handler args))) ...))))

;; Send message to chan or to nick if > max-help-lines
(define (priv-or-chan from chan message)
  (let* ((lines (string-split
                 (string-trim-right message #\newline)
                 #\newline))
         (to (if (> (length lines) max-help-lines) from chan)))
    (for-each (lambda (line) (bot:say to line)) lines)
    #t))

(define (show-apropos from chan message left right)
  (priv-or-chan from chan
                (with-output-to-string
                  (lambda ()
                    (apropos right))
                  #;(close (current-output-port)))))

(define (show-help from chan message left right)
  (priv-or-chan from chan
                (if right
                    (with-output-to-string
                      (lambda ()
                        ((@@ (ice-9 session) help-doc)
                         (string->symbol right)
                         (string-append "^" (regexp-quote right) "$"))
                        #;(close (current-output-port))))
                    "guile help <something>")))

(define (make-standard-infobot)
  (make-infobot
   ;; Plugins go here, in the form:
   ;; (crib help handler)
   ("botsnack"
    "Feed me!"
    (lambda args ":)"))
   ("apropos"
    "\"apropos <something>\" Display Guile apropos about the something."
    show-apropos)
   ("guile help "
    "\"guile help <something>\" Display Guile help about the something."
    show-help)
   ("what is "
    "\"What is <something>\" asks for information about the something."
    (*-is? 'what))
   ("what are "
    "\"What are <somethings>\" asks for information about the somethings."
    (*-is? 'what))
   ("where is "
    "\"Where is <something>\" asks for the location of the something."
    (*-is? 'where))
   ("where are "
    "\"Where are <somethings>\" asks for the location of the somethings."
    (*-is? 'where))
   ("who is "
    "\"Who is <someone>\" asks for information about someone."
    (*-is? 'what))

   ("later ask "
    "\"later ask <someone> <something>\" asks me to deliver a message to someone next time they speak."
    (tell 'later))
   ("later tell "
    "\"later tell <someone> <something>\" asks me to deliver a message to someone next time they speak."
    (tell 'later))
   ("tell "
    ;;"\"tell <someone> <something>\" asks me to deliver a message to someone as soon as I see them in a channel."
    "\"tell <someone> <something>\" asks me to deliver a message to someone right now."
    (tell 'now))
   ("seen "
    "\"seen <someone>\" asks for the last time I saw someone speak."
    seen)
   ("forget "
    "\"forget <something>\" asks me to forget what I know about something."
    forget)
   (" is also "
    "\"<fact> is also <something>\" tells me that the first term which I already know can also be named by the second."
    learn-aka)
   (" is aka "
    "\"<fact> is also <something>\" tells me that the first term which I already know can also be named by the second."
    learn-aka)
   (" is at "
    "\"<something> is at <somewhere>\" defines the location of a term for later recollection by \"where is\"."
    (learn 'where))
   (" is "
    "\"<something> is <something else>\" defines a term for later recollection by \"what is\"."
    (learn 'what))
   (" are "
    "\"<somethings> are <something>\" defines a term for later recollection by \"what are\"."
    (learn 'what))
   ("version"
    "Display version."
    (lambda args (simple-format #f
                           "Sneeky bot running on Guile version ~a using bobot++ ~a"
                           (version) (bot:getreleaserevision))))
   (#t
    "<something>? asks me what something is"
    (*-is? #f))
   ))

;; What is a "channel" here?
(define (infobot . plugins)
  (lambda (from to message)
    (let-values (((to-bot? cleaned-message strict-tokens)
                  (full-parse (bot:getnickname) message)))
      (when (string=? from to)          ; PRIVMESSAGE
        (set! to-bot? #t))
      (cond
       ((not to-bot?) #f)

       ((equal? cleaned-message "help") ; display help
        (bot:say from (format #f "Hello, I'm ~a, a Guile Scheme bobot++ bot. " (bot:getnickname)))
        (bot:say from "I respond to some natural language commands, such as:")
        (bot:say from "help - You're doing it.")

	(for-each
	 (lambda (p)
	   (bot:say from
		    (string-append
		     (if (eqv? (plugin-crib p) #t)
			 ""
			 (plugin-crib p))
		     " - " (plugin-help p))))
	 plugins)
        #f)

       ;; Here is the main loop
       (else
        (let loop ((p plugins))
          (cond
           ((null? p) #t)
           ;;  If the crib is #t or the crib matches the message
           ((or (eqv? #t (plugin-crib (car p)))
                (crib-match? (plugin-crib (car p)) message))
            (let ((rv (apply (plugin-handler (car p))
                             from
                             to
                             message
                             (crib-split (plugin-crib (car p)) cleaned-message))))
              ;; If the return value is a string, send it,
              ;; if it's 'continue, try the next thing in
              ;; the list.  Otherwise just return the value.
              (cond
               ((string? rv)
                (bot:say to rv)
                #f)
               ((eqv? rv 'continue)
                (loop (cdr p)))
               (else rv))))
           (else
            (loop (cdr p))))))))))
