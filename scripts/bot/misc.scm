
(define-module (bot misc)
  #:export (random-elem))

(define (random-elem ls)
  (list-ref ls (random (length ls))))
