
(define-module (bot sql)
  #:use-module (srfi srfi-13)
  #:use-module (sqlite)
  #:use-module (bobotpp bot) ;; bot:flushport bot:logport
  #:export (sql-query
            sql-command
            fix
            sql-open
            sql-close))

(define dbcon #f)

(define (sql-close)
  (when dbcon
    (sqlite-close dbcon)
    (set! dbcon #f)))

(define (sql-open)
  (set! dbcon (sqlite-open "/home/dsmith/sarah.db" #t)))


(define (sql-query statement . args)
  (let ((results '())
        (stmt (if (null? args)
                  statement
                  (apply format #f statement args))))
    ;;(format (bot:logport) "sql-query: ~s~%" stmt)
    ;;(bot:flushport)
    (sqlite-exec dbcon stmt
                 (lambda (n data names)
                   (set! results (cons data results))))
    (and (not (null? results))
         results)))

(define (sql-command statement . args)
  (let ((stmt (if (null? args)
                  statement
                  (apply format #f statement args))))
    ;;(format (bot:logport) "sql-command: ~s~%" stmt)
    ;;(bot:flushport)
    (sqlite-exec dbcon stmt (lambda (n data names) #f))))

;;; Double every ' character in S.
(define (fix str)
  (string-join (string-split str #\') "''"))
