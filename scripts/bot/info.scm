(define-module (bot info)
  #:use-module (srfi srfi-13)
  #:use-module (bot sql)
  #:use-module (bot misc)
  #:use-module (bot parsing)
  #:export (*-is?
            learn
            learn-aka
            forget))

;; basic infobot plugins
(define (*-is? type)
  (lambda (from channel message ignore term)
    (and term
         (let ((results (lookup-item type
                                     (trim (string-trim-right
                                            term #\?)))))
           (or (not results)
               (let ((random-result (random-elem results)))
                 (format #f "~a~a ~a ~a"
                         (random-elem whatis-preludes)
                         (vector-ref random-result 0)
                         (cdr (assq (string->symbol (vector-ref random-result 1))
                                    '((what . "is")
                                      (where . "is at"))))
                         (vector-ref random-result 2))))))))


(define (ignorable? tokens definition)
  (or (not definition)
      (and (> (length tokens) (length (tokenize definition)))
           (> (length tokens) 1))
      (and (= 1 (length tokens))
           (memq (car tokens) ignored-words))))

(define (learn type)
  (lambda (from channel message term definition)
    (if (not term)
        'continue
        (let ((tt (tokenize term)))
          (or (and (ignorable? tt definition)
                   'continue)
              (if (store-item (symbol->string type) term definition)
                  (random-elem learn-responses)
                  (random-elem knewthat-responses)))))))

(define (learn-aka from channel message term definition)
  (or (and (ignorable? (tokenize term) definition)
           'continue)
      (if (store-aka term definition)
          (random-elem learn-responses)
          (random-elem knewthat-responses))))

(define (forget from channel message ignore term)
  (remove-items term)
  (random-elem forget-responses))

;; Returns a list of (key type data) lists
;; Type appears to be 'what, 'where, or #f
(define (lookup-item type key)
  (sql-query (string-append
              "SELECT knowledge.key, knowledge.type, knowledge.data "
              "  FROM knowledge "
              "  LEFT JOIN aka "
              "         ON (aka.key like '~a' "
              "        AND aka.data like knowledge.key)"
              " WHERE ((aka.key IS NULL AND knowledge.key like '~a') "
              "    OR (aka.key IS NOT NULL)) "
              "   ~a;") (fix key) (fix key)
              (if type (format #f "AND type='~a'" type) "")))

;; This should return #f if the data was already in the table
(define (store-item type key data)
  (sql-command "INSERT INTO knowledge VALUES('~a','~a','~a');"
               (fix type) (fix key) (fix data))
  #t)

(define (store-aka data key)
  (sql-command "INSERT INTO aka VALUES('~a','~a');" (fix key) (fix data))
  #t)

(define (remove-items key)
  (sql-command "DELETE FROM knowledge WHERE key='~a';" (fix key)))

(define whatis-preludes
  '("I've heard "
    "Someone once said "
    ""
    "I could be wrong, but "
    "Its been said that "
    "Last time I checked "
    "From what I understand, "))

(define knewthat-responses
  '("Thats what I heard."
    "Yep, I know."
    "Thats very true."))

(define learn-responses
  '("Got it."
    "Okay."
    "Understood."
    "I'll keep that in mind."
    "So noted."))

(define forget-responses
  '("Okay."
    "Consider it forgotten."))

(define didntknow-responses
  '("Already done."
    "Never knew it."))

(define ignored-words
  '(about
    after
    all
    also
    an
    and
    another
    any
    are
    as
    at
    be
    $
    because
    been
    before
    being
    between
    both
    but
    by
    came
    can
    come
    could
    did
    do
    does
    each
    else
    for
    from
    get
    got
    has
    had
    he
    have
    her
    here
    him
    himself
    his
    how
    if
    in
    into
    is
    it
    its
    just
    like
    make
    many
    me
    might
    more
    most
    much
    must
    my
    never
    now
    of
    on
    only
    or
    other
    our
    out
    over
    re
    said
    same
    see
    should
    since
    so
    some
    still
    such
    take
    than
    that
    the
    their
    them
    then
    there
    these
    they
    this
    those
    through
    to
    too
    under
    up
    use
    very
    want
    was
    way
    we
    well
    were
    what
    when
    where
    which
    while
    who
    why
    will
    with
    would
    you
    your))
