
(define-module (bot parsing)
  #:use-module (bot metaphone)
  #:use-module (ice-9 format)
  #:use-module (ice-9 regex)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-13)
  #:use-module (bobotpp bot)
  #:export (tokenize
            crib-match?
            trim
            crib-split
            full-parse))

;; Used in into.scm, infobot.scm
(define trim string-trim-both)

;; Used in infobot.scm
(define (crib-split on str)
  (if (eq? on #t)
      (list "" (trim str))
      (let ((i (string-contains (string-downcase str) on)))
        (if i
            (list (trim (substring str 0 i))
                  (trim (substring str (+ i (string-length on)) (string-length str))))
            (list #f #f)))))

(define (tokenizer charset)
  (lambda (message)
    (map string->symbol
         (string-tokenize
          (string-filter
           charset
           (string-downcase message))))))

(define somewhat-clean-cs
  (string->char-set "abcdefghijklmnopqrstuvwxyz()[]->+-*/_0123456789?!. \t\n"))

;; Used in info.scm
(define tokenize (tokenizer somewhat-clean-cs))

(define really-clean-cs
  (string->char-set "abcdefghijklmnopqrstuvwxyz0123456789 \t\n"))

(define strict-tokenize (tokenizer really-clean-cs))

(define (bot-clean botnick message)
  (let* ((bot-rx (make-regexp (format #f "~a[:, ]*(.*)" botnick)))
         (m (regexp-exec bot-rx message)))
    (if m (match:substring m 1) message)))

;; Used in infobot.scm
(define (crib-match? crib message)
  (let ((strict-tokens (strict-tokenize message))
        (strict-crib (strict-tokenize crib)))
    (call/cc
     (lambda (escape)
       (let loop ((mt strict-tokens)  (ct strict-crib) (bt escape))
         (cond ((null? ct) (escape #t))
               ((null? mt) (bt #f))
               ((eq? (car mt) (car ct))
                (call/cc
                 (lambda (bt)
                   (loop (cdr mt) (cdr ct) bt)))
                (loop (cdr mt) strict-crib bt))
               (else (loop (cdr mt) strict-crib escape))))))))

;; Used in infobot.scm
(define (full-parse bot-name message)
  (let* ((strict-tokens (strict-tokenize message))
         (cleaned-message (bot-clean bot-name message))
         ;; Bot name must be first in a sentence
         (to-bot (or (and (pair? strict-tokens)
                          (string=? bot-name (symbol->string (car strict-tokens))))
                     ;; or last ..
                     (and (pair? strict-tokens)
                          (string=? bot-name (symbol->string (last strict-tokens)))))))
    (values to-bot cleaned-message strict-tokens)))
