;;; simple logging script
;;; Note that this doesn't get what the bot says
;;; and logs everything to the same file :(

(use-modules (ice-9 rdelim))

(define log-port (open-file "/home/dsmith/.bobotpp/logs/log2.log" "al"))

(define (make-fun text)
  (lambda args
    (write (cons (string->symbol text) args) log-port)
    (newline log-port)
    (force-output log-port)
    ))

;;(bot:addhook hooks/raw ".*" (make-fun "hooks/raw") 1000 #t "log")
;;(bot:addhook hooks/timer ".*" (make-fun "hooks/timer") 1000 #t "log")

;; RX
(bot:addhook hooks/action ".*" (make-fun "hooks/action") 1000 #t "log")
(bot:addhook hooks/ctcp ".*" (make-fun "hooks/ctcp") 1000 #t "log")
(bot:addhook hooks/ctcp-reply ".*" (make-fun "hooks/ctcp-reply") 1000 #t "log")
(bot:addhook hooks/disconnect ".*" (make-fun "hooks/disconnect") 1000 #t "log")
(bot:addhook hooks/flood ".*" (make-fun "hooks/flood") 1000 #t "log")
(bot:addhook hooks/invite ".*" (make-fun "hooks/invite") 1000 #t "log")
(bot:addhook hooks/join ".*" (make-fun "hooks/join") 1000 #t "log")
(bot:addhook hooks/kick ".*" (make-fun "hooks/kick") 1000 #t "log")
(bot:addhook hooks/leave ".*" (make-fun "hooks/leave") 1000 #t "log")
(bot:addhook hooks/message ".*" (make-fun "hooks/message") 1000 #t "log")
(bot:addhook hooks/mode ".*" (make-fun "hooks/mode") 1000 #t "log")
(bot:addhook hooks/nickname ".*" (make-fun "hooks/nickname") 1000 #t "log")
(bot:addhook hooks/notice ".*" (make-fun "hooks/notice") 1000 #t "log")
(bot:addhook hooks/part ".*" (make-fun "hooks/part") 1000 #t "log")
(bot:addhook hooks/public ".*" (make-fun "hooks/public") 1000 #t "log")
(bot:addhook hooks/public-notice ".*" (make-fun "hooks/public-notice") 1000 #t "log")
(bot:addhook hooks/raw ".*" (make-fun "hooks/raw") 1000 #t "log")
(bot:addhook hooks/signoff ".*" (make-fun "hooks/signoff") 1000 #t "log")
(bot:addhook hooks/topic ".*" (make-fun "hooks/topic") 1000 #t "log")

;; TX
(bot:addhook hooks/send/action ".*" (make-fun "hooks/send/action") 1000 #t "log")
(bot:addhook hooks/send/ctcp ".*" (make-fun "hooks/send/ctcp") 1000 #t "log")
(bot:addhook hooks/send/message ".*" (make-fun "hooks/send/message") 1000 #t "log")
(bot:addhook hooks/send/public ".*" (make-fun "hooks/send/public") 1000 #t "log")
(bot:addhook hooks/send/who ".*" (make-fun "hooks/send/who") 1000 #t "log")
(bot:addhook hooks/send/whois ".*" (make-fun "hooks/send/whois") 1000 #t "log")

;; DCC
(bot:addhook hooks/dcc/chat-begin ".*" (make-fun "hooks/dcc/chat-begin") 1000 #t "log")
(bot:addhook hooks/dcc/chat-end ".*" (make-fun "hooks/dcc/chat-end") 1000 #t "log")
(bot:addhook hooks/dcc/chat-message ".*" (make-fun "hooks/dcc/chat-message") 1000 #t "log")
